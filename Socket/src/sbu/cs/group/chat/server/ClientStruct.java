package sbu.cs.group.chat.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;

public class ClientStruct implements Runnable {
    private Socket socket;
    private final int id;
    private InputStream in;

    public int getId() {
        return id;
    }

    private OutputStream out;
    private ArrayBlockingQueue<Message> messages;
    private Thread thread;

    public ClientStruct(Socket socket, ArrayBlockingQueue<Message> messages
            , int id)
            throws IOException {
        this.socket = socket;
        this.id = id;
        this.messages = messages;
        out = socket.getOutputStream();
        in = socket.getInputStream();
        thread = new Thread(this);
        thread.start();
    }

    public void sendMessage(String message) throws IOException {
        out.write(message.getBytes());
        out.flush();
    }

    @Override
    public void run() {
        byte[] buffer = new byte[256];
        while (true){
            int len = 0;
            try {
                len = in.read(buffer);
                byte[] tmp = new byte[len];
                System.arraycopy(buffer, 0, tmp, 0, len);
                String message = new String(tmp);
                messages.offer(new Message(message, this));
                System.out.println("received message : " + message);
                buffer = new byte[256];
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
