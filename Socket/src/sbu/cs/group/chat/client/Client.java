package sbu.cs.group.chat.client;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        try {
            System.out.println("connecting...");
            Socket socket = new Socket("127.0.0.1", 5000);
            System.out.println("connected");
            System.out.println("establishing ClientListener");
            ClientListener cl = new ClientListener(socket.getInputStream());
            OutputStream out = socket.getOutputStream();
            System.out.println("ClientListener established");
            while (true) {
                System.out.println("enter any word...");
                byte[] buffer = in.next().getBytes();
                out.write(buffer);
                out.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
