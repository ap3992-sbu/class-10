package sbu.cs.tcp.client;

import java.io.IOException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		try {
			Client client = new Client("localhost", 5000, System.out);
			Scanner in = new Scanner(System.in);

			String message;
			do {
				message = in.nextLine();
				client.send(message);
				client.listen();
			}while(!message.equals("exit"));

			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
