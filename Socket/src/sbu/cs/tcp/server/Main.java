package sbu.cs.tcp.server;

import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		try {
			Server s = new Server(5000, System.out);
			s.acceptClient();

			String message;
			do {
				message = s.listen();
				s.send(message);
			}while(!message.equals("exit"));

			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
