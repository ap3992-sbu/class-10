package sbu.cs.tcp.server;

import java.io.*;
import java.net.*;

public class Server {
	private final int port;
	private ServerSocket ss;
	private Socket s;
	private BufferedReader in;
	private PrintWriter out;
	private PrintStream ps;

	public Server(int port, PrintStream ps) throws IOException {
		this.port = port;
		this.ps = ps;

		ps.println("Trying to stablish on port: " + port);
		ss = new ServerSocket(port);
		ps.println("Server is stablished");
	}

	public void acceptClient() throws IOException {
		ps.println("Waiting for a client to connect...");
		s = ss.accept();
		ps.println("Client connected");
		in = new BufferedReader(new InputStreamReader(s.getInputStream()));
		out = new PrintWriter(s.getOutputStream(), true);
	}

	public String listen() throws IOException {
		ps.println("Waiting for client message...");
		String message = in.readLine();
		ps.println("Message recived: " + message);
		return message;
	}

	public void send(String message) {
		out.println(message);
		ps.println("Message sent: " + message);
	}

	public void close() throws IOException {
		s.close();
		ss.close();
	}

	public void disconnect() throws IOException {
		s.close();
	}
}
