package sbu.cs.udp.client;

import sbu.cs.udp.server.UDPServer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class UDPClient {
    public final static int port = 7000;
    public static void main(String[] args) throws IOException {
        DatagramSocket socket = new DatagramSocket(UDPClient.port);
        Scanner in = new Scanner(System.in);
        byte[] buffer = in.next().getBytes();
        InetAddress address = InetAddress.getLocalHost();
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address
                , UDPServer.port);
        socket.send(packet);

        packet = new DatagramPacket(buffer, buffer.length);
        socket.receive(packet);
        System.out.println("got data from address : "
                + packet.getAddress().getHostAddress() + " port : "
                + packet.getPort()
                + " : " + new String(packet.getData()));
        socket.close();
    }
}
