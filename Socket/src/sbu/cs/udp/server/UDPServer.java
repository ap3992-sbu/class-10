package sbu.cs.udp.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDPServer {
    public final static int port = 6000;

    public static void main(String[] args) throws IOException {
        DatagramSocket socket = new DatagramSocket(UDPServer.port);
        byte[] buffer = new byte[256];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        socket.receive(packet);
        InetAddress address = packet.getAddress();
        int port = packet.getPort();
        System.out.println("got data from address : "
                + address.getHostAddress() + " port : " + port
                + " : " + new String(packet.getData()));
        packet = new DatagramPacket(buffer, buffer.length, address, port);
        socket.send(packet);
        socket.close();
    }
}
